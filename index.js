console.log("Hello World")

	let num1 = 25;
	let num2 = 5;
	
	console.log("The result of num1 + num2 should be 30.");

	console.log("Actual Result:");

	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;

	console.log("The result of num3 + num4 should be 200.");

	console.log("Actual Result:");

	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;

	console.log("The result of num5 - num6 should be 7.");

	console.log("Actual Result:");

	console.log(num5-num6);
		
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let resultMinutes = daysYear * hoursDay * minutesHour

	console.log("There are " + resultMinutes + " minutes in a year.")

	let tempCelsius = 132;
	const Fa = 9/5
	const Fb = 32
	let resultFahrenheit = (tempCelsius * Fa) + Fb

	console.log(tempCelsius + " degrees Celsius when converted to Fahrenheit is " + resultFahrenheit)

	let num7 = 165;
	let isDivisibleBy8 = num7 % 8

	console.log("The remainder of " + num7 + " divided by 8 is: " + isDivisibleBy8)
	
	console.log("Is num7 divisible by 8?");

	console.log(isDivisibleBy8 === 0)
	
	let num8 = 348;
	let isDivisibleBy4 = num8 % 4
	console.log("The remainder of " + num8 + "divided by 4 is: " + isDivisibleBy4)
	
	console.log("Is num8 divisible by 4?");

	console.log(isDivisibleBy4 === 0)	